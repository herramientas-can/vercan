.\"
.\"    Copyright 2019 Miguel A. Pérez Valdenebro (miguelangel.perezvaldenebro@wilbord.com)
.\"
.\"    This file is part of verCAN
.\"
.\"    verCAN is free software: you can redistribute it and/or modify
.\"    it under the terms of the GNU General Public License as published by
.\"    the Free Software Foundation, either version 3 of the License, or
.\"    (at your option) any later version.
.\"
.\"    verCAN is distributed in the hope that it will be useful,
.\"    but WITHOUT ANY WARRANTY; without even the implied warranty of
.\"    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\"    GNU General Public License for more details.
.\"
.\"    You should have received a copy of the GNU General Public License
.\"    along with verCAN  If not, see <https://www.gnu.org/licenses/>.
.\"
.TH verCAN 1 "16 Oct 2019" "1.0" "Página de manual de verCAN"
.
.SH NOMBRE
verCAN \- representa graficamente la información del bus CAN
.
.SH SINOPSIS
.B verCAN
[OPCIONES]
.
.SH DESCRIPCION
verCAN es una utilidad que permite visualizar un conjunto de los datos de los ficheros generados por candump a través de gnuplot, o conectarse en tiempo real al bus CAN especificado y remitir esa información via UDP a un servidor en tiempo real, para poderlos mostrar con herramientas del tipo Telemetry Viewer.
.SH OPCIONES
verCAN soporta las siguientes opciones:
.SS Entrada de datos
.TP
.BR "-f FICHERO"
Especifica el fichero de entrada con el que se desea trabajar
.TP
.BR "-i INTERFAZ CAN"
Especifica el interfaz CAN del cual queremos leer los datos (p.e. can0).
.br
Las opciones -f y -i son mutuamente excluyentes y una de ellas siempre tiene que estar presente.
.br
Si la opción -i está presente, se espera que esté también presente la opción -o.
.SS Salida de datos
.TP
.BR "-o SERVIDOR[:PUERTO UDP]"
Especifica el servidor donde reside Telemetry viewer. Opcionalmente puede especificarse un puerto UDP. En caso de no hacerlo, se usará el puerto 8080.
.br
Esta opción es obligatoria si se ha seleccionado la opción -i como entrada de datos.
.br
De no especificarse esta opción y haber fijado la entrada con -f, la salida se mandará a gnuplot.
.br
Esta opción también crea un fichero de configuración de Telemetry Viewer que facilita la visualización de los datos en el mismo.
.SS Definición de datos CAN a mostrar
.TP
.BR "-d '<CANID>#<DATOS>[|<DATOS]>'"
Es un parámetro que especifica como trabajar con los datos de las tramas CAN.
.RS
.TP
.B CANID
determina el identificador en hexadecimal de la trama CAN con la que se quiere trabajar
.TP
.B DATOS
determina los bytes de la trama de datos CAN con los que se va a trabajar. Tiene el siguiente formato:
.RS
.RS
.TP
[F|B] [U|S] [+|-] (0-7) [ (<|+|-|*) (0-7) ] [ & máscara ]
.RE
.TP
Donde los términos entre paréntesis () son obligatorios y los términos entre corchetes [] son opcionales.
.RS
.TP
.B [F|B]
significa que es opcional la presencia de una F si deseamos aplicar una media movil a los datos o bien B, si los deseamos procesar los datos en bruto. De no especificar nada se usará la opción de datos en bruto.
.TP
.B [U|S]
significa que es opcional la presencia de una U si deseamos los datos sin signo o bien S, si los deseamos con signo. De no especificar nada se usará la opción sin signo.
.TP
.B [+|-]
significa si queremos los datos en su valor positivo (+) o negativo (-). Es un valor opcional y en caso de omitirse se mostrarán los datos con su valor positivo.
.TP
.B (0-7)
significa que se espera una cifra entre 0 y 7, que hace referencia al byte de datos de la trama CAN.
.TP
.B (<|+|-|*)
indica que se espera uno de esos operadores, donde:
.RS
.TP
.B <
operador palabra. Se utiliza para conformar una palabra, es decir, 4<5 significa, la palabra de 16 bits compuesta por el byte 4 (más significativo) y 5 (menos significativo).
.TP
.B +
operador suma
.TP
.B -
operador resta
.TP
.B *
operador multiplicación
.RE
.TP
.B & máscara 
realiza la operación & a nivel de bit con el número hexadecimal máscara. 
.RE
.TP
Pueden especificarse más de una operación DATOS para el mismo CANID, separándose entre ellas por el separador |
.RE
.TP
Del mismo modo, puede haber tantas entradas -d '' como se necesiten.
.RE
.SS Definición de datos OBDII a mostrar
.TP
.BR "-O '<PID OBDII>#<DATOS>[|<DATOS]>'"
Es un parámetro que especifica como trabajar con los datos de OBDII (identificados por el CAN ID 0x7E8).
.RS
.TP
.B PID OBDII
determina el identificador de parámetro del servicio 0x1 de OBDII.
.TP
.B DATOS
determina los bytes de la trama de datos CAN con los que se va a trabajar. Tiene el mismo formato que en la opción -d. No soporta tramas extendidas (CAN FD) por lo que la longitud de la respueta tiene que entrar en los 5 bytes disponibles y no se comprueba que eso ocurra. 
.SS Periodos de tiempo
Solo funcionan con la visualización de datos desde fichero (opción -f)
.TP
.BR "-A INICIO" 
Permite establecer el tiempo (en segundos) de inicio de la visualización de datos, descartando los valores anteriores.
.TP
.BR "-Z FIN" 
Permite establecer el tiempo (en segundos) de finalización de la visualización de datos, descartando los valores posteriores
.SS Otros parámetros
.TP
.BR "-2"
Es un parámetro opcional. Si se especifica, verCAN agrupará de dos en dos las gráficas dentro de una misma opción -d, siempre y cuando haya más de una, utilizando los ejes izquierdo y derecho para representar sus valores.
.TP
.BR "-r"
Es un parámetro opcional. Si se especifica se eliminan los ficheros de datos temporales creados por verCAN durante su funcionamiento, fichero que permanecerá en caso de que este parámetro no se use.
.TP
.BR "-m VALOR"
Permite modificar el numero de elementos utilizados para calcular la media movil entre 2 y 20, en el caso de que se esté usando la opción F en alguno de los DATOS. Si no se especifica se usa un valor por defecto de 5 elementos.
.SH EJEMPLOS
.TP
.BR "verCAN -i can0 -o 192.168.1.7:8030 -d'0D0#1<2|S0' -d'0D1#0&3'"
Selecciona de los datos recibidos por el interfaz can0, de aquellos con CANID 0x0D0, la palabra, sin signo, compuesta por los bytes b2 (más significativo) y b1 (menos significativo) y el byte b0 con signo; y de los que tengan CANID 0x0D1, seleciona los dos bits menos significativos del byte b0 (b0 & 0x03), y envía los datos por UDP al servidor 192.168.1.7 por el puerto UDP 8030.
.TP
.BR "verCAN -f candump-2019-06-21_183005.log -d'137#F0<1|3<2' -2 -r -A 10 -Z30 -m 8
Selecciona de los datos del fichero candump-2019-06-21_183005.log (generado por candump(1)), de aquellos que se encuentren entre los segundos 10 y 30, y de aquellos con CANID 0x137, la palabra compuesta por los bytes b0 (más significativo) y b1 (menos significativo), aplicándole una media movil de 8 elementos, y la palabra compuesta por los bytes b3 (más significativo) y b2 (menos significativo), representando los datos en la misma gráfica (mediante gnuplot(1)), y eliminando todos los ficheros auxiliares generados. 
.SH VER TAMBIÉN
candump(1), gnuplot(1), Telemetry Viewer
.SH BUGS
No se conocen bugs.
.SH AUTOR
Miguel Angel Pérez Valdenebro (miguelangel.perezvaldenebro@wilbord.com)

