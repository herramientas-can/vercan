/*
    verCAN

    Copyright 2019 Miguel A. Pérez Valdenebro (miguelangel.perezvaldenebro@wilbord.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

/*
 * Control de cambios
 *
 * 06/03/2021	mapv	Añadida opción -l para visualización de las gráficas en modo 'linespoints'
 * 			Añadida opción -p para visualizar distintos tipos de puntos
 * 			Añadida opción -E para seleccionar el CAN ID de la ECU en el modo OBDII
 * 			Añadida opción -S para seleccionar el servicio a visualizar en el modo OBDII
 */

/*
 * Includes
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <limits.h>
#include <ctype.h>
#include <assert.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/time.h>

#include <linux/can.h>
#include <linux/can/raw.h>
#include <netinet/in.h>
#include <netdb.h>
#include <net/if.h>
#include <arpa/inet.h>


/*
 * Definiciones
 */
#define VC_PARAMS	32
#define VC_OPS		16
#define VC_PROMEDIO	20
#define VC_PROMEDIO_MIN	2

#define VC_PROMEDIO_INI	2

#define UDP_PORT	8080
#define UDP_DATA	1024

#define	TV_INIT		"Telemetry Viewer File Format v0.6\n"\
			"\n"\
			"GUI Settings:\n"\
			"\n"\
			"	tile column count = 6\n"\
			"	tile row count = 6\n"\
			"	show plot tooltips = true\n"\
			"	smooth scrolling = true\n"\
			"	opengl antialiasing = true\n"\
			"	show fps and period = false\n"\
			"	chart index for benchmarks = -1\n"\
			"\n"\
			"Communication Settings:\n"\
			"\n"\
			"	port = UDP\n"\
			"	uart baud rate = 9600\n"\
			"	tcp/udp port number = %d\n"\
			"	packet type = CSV\n"\
			"	sample rate = 1000\n"\
			"\n"

#define TV_LOC		"%d Data Structure Locations:\n\n"

#define TV_LOCATION	"	location = %d\n"\
			"	processor index = -1\n"\
			"	name = %s#%s%s\n"\
			"	color = 0x%06X\n"\
			"	unit = \n"\
			"	conversion factor a = 1.0\n"\
			"	conversion factor b = 1.0\n"\
			"\n"

#define TV_END		"Checksum:\n"\
			"\n"\
			"	location = -1\n"\
			"	checksum processor index = -1\n"\
			"\n"\
			"0 Charts:\n"

#define	TV_BUFFER_LEN	(1024*16)

/*
 * Definición de tipos
 */
typedef	struct opers
{
	char *ops;
	int negativo;
	int signo;
	int op1;
	int op2;
	int mask;
	char operador;
	char * manejador;
	int filtrado;
	int npromedio;
	int indice_p;
	float promedio[VC_PROMEDIO];
	float media;
} opers_t;

typedef struct params
{
	int obdii_flag;
	char * pid;				/* CAN ID - char */
	u_int16_t npid;				/* CAN ID - int */
	u_int16_t obdii_pid;			/* CAN ID - int */
	char * token;
	FILE * oarchivo;			/* Descriptor del fichero de salida de datos */
	char * nom_fichero;			/* Nombre del fichero de salida de datos */
	int nops;				/* Número de operaciones */
	opers_t operaciones[VC_OPS];		/* Array de datos de la operaciones */
	float (*operador[VC_OPS])(opers_t *, u_int8_t * );	/* Array de punteros a las funciones para gestionar las operaciones */
	u_int8_t byte[8];			/* Datos de la trama CAN */
} params_t;
	
static int sk;                          /* Descriptor del socket del bus CAN */
static int sockfd;                      /* Descriptor del socket de la conexión UDP */
static struct sockaddr_in serv;         /* Datos del servidor UDP */


int genera_color( void )
{
	return rand() % 0xFFFFFF;
}

/*
 * Creación del socket e iniciación del bus CAN
 */
static void canbus_init(char *ifname)
{
        int recv_own_msgs;
        struct sockaddr_can addr;
        struct ifreq ifr;

        /*
	 * Creamos el descriptor del socket CAN
	 * */

        sk = socket( PF_CAN, SOCK_RAW, CAN_RAW );

        if (sk < 0)
	{
                perror("Fallo en la creación del socket CAN bus");
		printf("Interfaz: %s\n", ifname );
                exit(102);
        }

        memset(&ifr.ifr_name, 0, sizeof(ifr.ifr_name));
        strncpy(ifr.ifr_name, ifname, IFNAMSIZ);

        if (ioctl(sk, SIOCGIFINDEX, &ifr) < 0)
	{
                perror("Fallo en el ioctl(SIOCGIFINDEX)");
		printf("Interfaz: %s\n", ifname );
                close(sk);
                exit(103);
        }

        memset(&addr, 0, sizeof(addr));
        addr.can_family = AF_CAN;
        addr.can_ifindex = ifr.ifr_ifindex;

        if (bind(sk, (struct sockaddr *)&addr, sizeof(addr)) < 0)
	{
                perror("Fallo en el bind() al CAN bus");
		printf("Interfaz: %s\n", ifname );
                close(sk);
                exit(104);
        }

        recv_own_msgs = 0; /* 0 = disabled (default), 1 = enabled */
        setsockopt(sk, SOL_CAN_RAW, CAN_RAW_RECV_OWN_MSGS, &recv_own_msgs, sizeof(recv_own_msgs));

        return;
}


/*
 * Creación del socket e iniciación de la conexión UDP
 */
static void udp_init( char *hostname, u_int16_t port )
{
    struct hostent *host;

   /*
    * Creamos el descriptor del socket UDPs
    */
    sockfd = socket(AF_INET, SOCK_DGRAM, 0);

    if ( sockfd < 0 )
    {
        perror("Fallo en la creación del socket UDP");
        exit(105);
    }

    memset(&serv, 0, sizeof(serv));

    host = gethostbyname( hostname );

    if( host == NULL )
    {
            perror("No puedo obtener la configuración del servidor");
            fprintf(stderr, "Hostname: %s\n", hostname);
            close(sockfd);
            exit(106);
    }

    /*
     * Informamos de los datos del servidor
    */
    serv.sin_family = AF_INET;
    serv.sin_port = htons( port );
    serv.sin_addr = *((struct in_addr *) host->h_addr);

    return;
}


/*
 * Gestión de la media movil de la señal
 */
float promedia( int valor, opers_t * op )
{
	op->media = op->media + ( ( valor - op->promedio[op->indice_p] ) / op->npromedio );

	op->promedio[op->indice_p] = valor;
	op->indice_p++;
	if( op->indice_p == op->npromedio )
		op->indice_p = 0;

	return op->media;
}

/*
 * Manejadores de operaciones
 *
 * Operador byte
 */
float operador_byte( opers_t * op, u_int8_t * data )
{
	float rc;

	if( op->signo == 1)
		rc = (int8_t)data[op->op1] * op->negativo;
	else
		rc = ( ( data[op->op1] * op->negativo ) & op->mask );

	if( op->filtrado == 1 )
		rc = promedia( rc, op );

	return rc;
}

/*
 * Operador palabra
 */
float operador_palabra( opers_t * op, u_int8_t *data )
{
	float rc;

	if( op->signo == 1)
	{
		int16_t w;

		w = (int16_t)( (data[op->op1] << 8 ) |  data[op->op2] );

		rc = (float)w * op->negativo;
	}
	else
		rc = (( data[op->op1] * 256 +  data[op->op2] ) * op->negativo ) & op->mask;

	if( op->filtrado == 1 )
		rc = promedia( rc, op );

	return rc;
}

/*
 * Operador suma
 */
float operador_suma( opers_t * op, u_int8_t * data )
{
	float rc;

	if( op->signo == 1)
	{
		int8_t d1, d2;
		int16_t w;

		d1 = (int8_t)data[op->op1];
		d2 = (int8_t)data[op->op2];

		w = d1 + d2;

		rc =  w * op->negativo;
	}
	else
		rc = (( data[op->op1] +  data[op->op2] ) * op->negativo ) & op->mask;

	if( op->filtrado == 1 )
		rc = promedia( rc, op );

	return rc;
}

/*
 * Operador resta
 */
float operador_resta( opers_t * op, u_int8_t * data )
{
	float rc;

	if( op->signo == 1)
	{
		int8_t d1, d2;
		int16_t w;

		d1 = (int8_t)data[op->op1];
		d2 = (int8_t)data[op->op2];

		w = d1 - d2;

		rc = w * op->negativo;
	}
	else
		rc = (( data[op->op1] -  data[op->op2] ) * op->negativo ) & op->mask;

	if( op->filtrado == 1 )
		rc = promedia( rc, op );

	return rc;
}

/*
 * Operador producto
 */
float operador_producto( opers_t * op, u_int8_t * data )
{
	float rc;

	if( op->signo == 1)
	{
		int8_t d1, d2;
		int16_t w;

		d1 = (int8_t)data[op->op1];
		d2 = (int8_t)data[op->op2];

		w = d1 * d2;

		return (w * op->negativo);
	}

	rc = (( data[op->op1] *  data[op->op2] ) * op->negativo ) & op->mask;

	if( op->filtrado == 1 )
		rc = promedia( rc, op );

	return rc;
}

/*
 * Operador AND (para aplicar la máscara)
 */
float operador_and( opers_t * op, u_int8_t * data )
{
	float rc;

	rc = (data[op->op1] & op->mask ) * op->negativo;

	if( op->filtrado == 1 )
		rc = promedia( rc, op );

	return rc;
}

/*
 * Función principal
 *
 * Argumentos:
 *
 * Obligatorio
 *
 * -f  <fichero a tratar>
 * 
 */
int main(int argc, char** argv )
{
 	FILE *iarchivo;
 	FILE *sarchivo;

 	char caracteres[128];
 	char argnom[100];
 	char fnom[120];
 	char commsnom[100];
	long base = 0;

	char c;
	char *endptr;

	char tv_buffer[TV_BUFFER_LEN];

	int i, salidas = 0;
	
	char trazo[24] = "lines";

	int iflag = 0;
	int oflag = 0;
	int fflag = 0;
	int rflag = 0;
	int flag2 = 0;
	int debug = 0;

	char _tiempo_i[20];
	char _tiempo_f[20];
	int tiempo_i = 0;
	int tiempo_f = INT_MAX;

	char _mpromedio[5];
	int mpromedio = 5;

	char _tipo_punto[5];
	int tipo_punto;

	char _ecupid[16];
 	int ecupid = 0x7E8;

	char _oservice[16];
 	int oservice = 0x01;

	params_t parametros[VC_PARAMS];

	/*
	 * Fija una semilla para la generación de números aleatorios a partir del número de proceso
	 */
	srand( getpid() );

	/*
	 * Tratamiento de argumentos
	 */
        while ( (c = getopt (argc, argv, "A:Z:E:S:O:f:i:o:m:d:p:2rtl") ) != -1)
	{
                switch (c)
                {
                        case 'f':
                                strcpy( argnom, optarg );
				fflag = 1;
				if( iflag == 1 )
				{
					printf("Error: No se puede especificar un fichero de entrada si se ha especificado un interfaz CAN\n"); 
					exit(80);
				}

                                break;

	 		case 'i':
                                strcpy( argnom, optarg );
				iflag = 1;
				if( rflag == 1 )
				{
					printf("Error: No se puede especificar un interfaz CAN si se ha especificado un fichero de entrada\n");
					exit(81);
				}

                                break;

                        case 'o':
                                strcpy( commsnom, optarg );
				oflag = 1;

                                break;
				
                        case 'l':
				strcpy( trazo, "linespoints" );

                                break;

                        case 'p':
				strcpy( trazo, "points" );
                                strcpy( _tipo_punto, optarg );
				tipo_punto = strtol( _tipo_punto, &endptr, 10 ); /* Base 10 */
				sprintf( trazo, "points pointtype %d", tipo_punto );


                                break;

                        case 'm':
                                strcpy( _mpromedio, optarg );
				mpromedio = strtol( _mpromedio, &endptr, 10 ); /* Base 10 */

                                /* Validación de posibles errores */

                                if( (errno == ERANGE || (errno != 0 && mpromedio == 0)) )
                                {
                                        perror("strtol");
                                        exit(EXIT_FAILURE);
                                }

                                if( endptr == _mpromedio )
                                {
                                        fprintf(stderr, "No se encontró valor para la media móvil\n");
                                        exit(EXIT_FAILURE);
                                }

                                if( mpromedio > VC_PROMEDIO )
                                {
                                        fprintf(stderr, "El valor promedio de la media móvil (%d) no puede ser mayor de %d. Usando %d\n", mpromedio, VC_PROMEDIO, VC_PROMEDIO);
					mpromedio = VC_PROMEDIO;
                                }
				else if ( mpromedio < VC_PROMEDIO_MIN )
                                {
                                        fprintf(stderr, "El valor promedio de la media móvil (%d) no puede ser menor de %d. Usando %d\n", mpromedio, VC_PROMEDIO_MIN, VC_PROMEDIO_MIN);
					mpromedio = VC_PROMEDIO_MIN;
                                }

                                break;

                        case 'r':
                                rflag = 1;

                                break;

                        case '2':
                                flag2 = 1;

                                break;

                        case 't':
                                debug = 1;

                                break;

                        case 'd':
				parametros[salidas].obdii_flag = 0;
				parametros[salidas].pid = strdup( optarg );
				salidas++;

				break;

                        case 'O':
				parametros[salidas].obdii_flag = 1;
				parametros[salidas].pid = strdup( optarg );
				salidas++;

                                break;

                        case 'E':
                                strcpy( _ecupid, optarg );
                                ecupid = strtol( _ecupid, &endptr, 16 ); /* Base 16 */

                                /* Validación de posibles errores */

                                if( (errno == ERANGE || (errno != 0 && ecupid == 0)) )
                                {
                                        perror("strtol");
                                        exit(EXIT_FAILURE);
                                }

                                if( endptr == _ecupid )
                                {
                                        fprintf(stderr, "No se encontró valor para el identificador de la ECU\n");
                                        exit(EXIT_FAILURE);
                                }

                                break;

                        case 'S':
                                strcpy( _oservice, optarg );
                                oservice = strtol( _oservice, &endptr, 16 ); /* Base 16 */

                                /* Validación de posibles errores */

                                if( (errno == ERANGE || (errno != 0 && oservice == 0)) )
                                {
                                        perror("strtol");
                                        exit(EXIT_FAILURE);
                                }

                                if( endptr == _oservice )
                                {
                                        fprintf(stderr, "No se encontró valor para el servicio OBDII\n");
                                        exit(EXIT_FAILURE);
                                }

                                break;

                        case 'A':
                                strcpy( _tiempo_i, optarg );
				tiempo_i = strtol( _tiempo_i, &endptr, 10 ); /* Base 10 */

                                /* Validación de posibles errores */

                                if( (errno == ERANGE || (errno != 0 && tiempo_i == 0)) )
                                {
                                        perror("strtol");
                                        exit(EXIT_FAILURE);
                                }

                                if( endptr == _tiempo_i )
                                {
                                        fprintf(stderr, "No se encontró valor para el tiempo inicial\n");
                                        exit(EXIT_FAILURE);
                                }

                                break;

                        case 'Z':
                                strcpy( _tiempo_f, optarg );
				tiempo_f = strtol( _tiempo_f, &endptr, 10 ); /* Base 10 */

                                /* Validación de posibles errores */

                                if( (errno == ERANGE || (errno != 0 && tiempo_f == 0)) )
                                {
                                        perror("strtol");
                                        exit(EXIT_FAILURE);
                                }

                                if( endptr == _tiempo_f )
                                {
                                        fprintf(stderr, "No se encontró valor para el tiempo inicial\n");
                                        exit(EXIT_FAILURE);
                                }

                                break;


		        default:
                                abort ();
                }
	}

	/*
	 * Análisis sintáctico de las salidas a generar, a partir de los argumentos introducidos (opciones -d)
	 */
	if( debug == 1 )
		printf("salidas %d\n", salidas);

	for( i = 0; i < salidas; i++ )
	{
		char s[3] = "|";
		char *ptr;
		int op=0;
		int j;

		parametros[i].token = strchr( parametros[i].pid, '#' );
		if( parametros[i].token == NULL )
		{
			printf(" >>> %s error de sintaxis\n", parametros[i].pid );
			exit(4+i);
		}

		*parametros[i].token='\0';

		if( parametros[i].obdii_flag == 1 )
		{
			parametros[i].obdii_pid = strtol( parametros[i].pid, &endptr, 16 ); /* Base 16 */

			parametros[i].npid = ecupid;
			parametros[i].pid = _ecupid;

                	/*
			 * Validación de posibles erroress
			 */

                	if( (errno == ERANGE || (errno != 0 && parametros[i].obdii_pid == 0)) )
                	{
                         	perror("strtol");
                         	fprintf(stderr, "Error: OBDII PID inválido\n");
                         	exit(EXIT_FAILURE);
                	}

                	if( endptr == parametros[i].pid )
                	{
                         	fprintf(stderr, "No se encontró valor para el PID OBDII\n");
                         	exit(EXIT_FAILURE);
                	}

			if( parametros[i].obdii_pid >= 0x100 )
                	{
                         	fprintf(stderr, "PID OBDII overflow\n");
                         	exit(EXIT_FAILURE);
                	}
		}
		else
		{
			parametros[i].npid = strtol( parametros[i].pid, &endptr, 16 ); /* Base 16 */
			parametros[i].obdii_pid = 0x00;

                	/*
			 * Validación de posibles erroress
			 */

                	if( (errno == ERANGE || (errno != 0 && parametros[i].pid == 0)) )
                	{
                         	perror("strtol");
                         	fprintf(stderr, "Error: CAN ID inválido\n");
                         	exit(EXIT_FAILURE);
                	}

                	if( endptr == parametros[i].pid )
                	{
                         	fprintf(stderr, "No se encontró valor para el CAN ID\n");
                         	exit(EXIT_FAILURE);
                	}

			if( parametros[i].npid >= 0x800 )
                	{
                         	fprintf(stderr, "CAN ID overflow\n");
                         	exit(EXIT_FAILURE);
                	}
		}

		parametros[i].token++;

		if( debug == 1 )
		{
			printf(">>>\nCAN ID     %s\n", parametros[i].pid );
			printf("OBDII_flag %d\n", parametros[i].obdii_flag );
			printf("OBDII PID %02X\n", parametros[i].obdii_pid );
		}

		ptr = strtok( parametros[i].token, s );

		parametros[i].operaciones[op].ops = ptr;	
		op++;

		while( ( ptr = strtok( NULL, s) )  != NULL )
		{
			parametros[i].operaciones[op].ops = ptr;	
			op++;
		}

		parametros[i].nops = op;

		/*
	 	 * Análisis sintáctico de los datos solicitados
	 	 */
		for( j = 0; j < parametros[i].nops; j++ )
		{
			parametros[i].operaciones[j].mask = 0xFFFF;
			ptr = parametros[i].operaciones[j].ops;

			/*
			 * Filtrado de la señal (por media móvil) o datos en bruto  ... opcional.
			 * Si no se especifica, se usarán datos en bruto.
			 */
			if( *ptr == 'F' || *ptr == 'f' )
			{
				int k;

				parametros[i].operaciones[j].filtrado = 1;
				parametros[i].operaciones[j].indice_p = 0;
				parametros[i].operaciones[j].media = 0;
				parametros[i].operaciones[j].npromedio = mpromedio;

				for( k = 0; k < VC_PROMEDIO; k++ )
					parametros[i].operaciones[j].promedio[k] = 0;

				ptr++;
			}
			else if( *ptr == 'B' || *ptr == 'b' )
			{
				parametros[i].operaciones[j].filtrado = 0;
				ptr++;
			}
			else
				parametros[i].operaciones[j].filtrado = 0;

			/*
			 * Con o sin signo ... opcional
			 * Si no se especifica, se usará sin signo
			 */
			if( *ptr == 'U' || *ptr == 'u' )
			{
				parametros[i].operaciones[j].signo = 0;
				ptr++;
			}
			else if( *ptr == 'S' || *ptr == 's' )
			{
				parametros[i].operaciones[j].signo = 1;
				ptr++;
			}
			else
				parametros[i].operaciones[j].signo = 0;

			/*
			 * + o - ... opcional
			 * Si no se especifica, se usará +
			 */
			if( *ptr == '-' )
			{
				ptr++;
				parametros[i].operaciones[j].negativo = -1;
			}
			else if( *ptr == '+' )
			{	
				ptr++;
				parametros[i].operaciones[j].negativo = 1;
			}
			else
				parametros[i].operaciones[j].negativo = 1;

			/*
			 * Primer byte
			 */
			if( !isdigit( *ptr ) )
			{
				printf("Error de sintaxis. Se esperaba una cifra entre 0 y 7\n");
				exit(40);
			}

			if( *ptr == '8' || *ptr == '9' )
			{
				printf("Error de sintaxis. Se esperaba una cifra entre 0 y 7\n");
				exit(41);
			}

			parametros[i].operaciones[j].op1 = *ptr -'0';

			/* Siguiente caracter */
			ptr++;

			if( *ptr == '\0' )
			{
				char aux[40];

				parametros[i].operaciones[j].operador = ' ';
				sprintf( aux, "D%d", parametros[i].operaciones[j].op1 );

				parametros[i].operaciones[j].manejador = strdup( aux );

				parametros[i].operador[j] = operador_byte; 
			}
			else
			{
				char aux[40];

				parametros[i].operaciones[j].operador = *ptr;

				switch (*ptr)
				{
					case '<':
						parametros[i].operador[j] = operador_palabra; 

						break;

					case '&':
						if( parametros[i].operaciones[j].signo == 1 )
						{
							printf("La operación & solo es compatible con datos sin signo (U)\n");
							exit(51);
						}

						parametros[i].operador[j] = operador_and; 

						break;	
			
					case '+':

						parametros[i].operador[j] = operador_suma; 

						break;	
			
					case '-':

						parametros[i].operador[j] = operador_resta; 

						break;	
			
					case '*':

						parametros[i].operador[j] = operador_producto; 
						break;	
			
					default:
						printf("Error de sintaxis. Se esperaba un operador (<, &, +, -, *)\n");
						exit(45);
				}

				if( *ptr != '&' )
				{
					ptr++;

					if( isdigit( *ptr ) )
					{
						if( *ptr == '8' || *ptr == '9' )
						{
							printf("Error de sintaxis. Se esperaba una cifra entre 0 y 7\n");
							exit(42);
						}
						parametros[i].operaciones[j].op2 = *ptr -'0';
					}
					else
					{
						printf("Error de sintaxis. Se esperaba una cifra\n");
						exit(43);
					}
					ptr++;
				}

				if( *ptr == '&' )
				{
					char *aux;

					if( parametros[i].operaciones[j].signo == 1 )
					{
						printf("La operación & solo es compatible con datos sin signo (U)\n");
						exit(51);
					}
					ptr++;

					aux = ptr;

					while( isxdigit(*aux) )
						aux++;

					if( *aux != '\0' )
					{
						printf("Error de sintaxis. Caracter no esperado ...\n");
						exit(42);
					}
				
					parametros[i].operaciones[j].mask = strtol( ptr, &aux, 16 );
				}

				sprintf( aux, "D%d%cD%d", parametros[i].operaciones[j].op1, parametros[i].operaciones[j].operador, parametros[i].operaciones[j].op2 );
				parametros[i].operaciones[j].manejador = strdup( aux );
			}

			if( debug == 1 )
			{
				printf("\n	@ Token     = %s\n\n", parametros[i].operaciones[j].ops );
				printf("	@ Op1       = %d\n",   parametros[i].operaciones[j].op1 );
				printf("	@ Manejador = %s\n",   parametros[i].operaciones[j].manejador );
				printf("	@ Op2       = %d\n",   parametros[i].operaciones[j].op2 );
				printf("	@ Mascara   = %04X\n", parametros[i].operaciones[j].mask );
				printf("	@ Signo     = %d\n",   parametros[i].operaciones[j].signo );
				printf("	@ Negativo  = %d\n",   parametros[i].operaciones[j].negativo );
			}
		}
	}

	/*
	 * Gestión del interfaz de entrada de datos: Apertura de uno de los dos interfaces
	 */
	if( fflag == 1 )
	{
		/*
	 	 * Apertura del fichero a tratar
	 	 */
 		iarchivo = fopen( argnom,"r");
 		if (iarchivo == NULL )
		{
			perror("Error: ");
 			exit(31);
		}
	}
	else if ( iflag == 1 )
	{
		if( oflag == 0 )
		{
			printf("Debe especificarse un servidor UDP: -o servidor[:puerto]\n");
			exit(32);
		}

		/*
	 	 * Inicializamos el bus CAN
	 	 */
		canbus_init( argnom );
	}
	else
	{
		printf("Debe especificarse una opción de entrada de datos: -f o -i\n");
		exit(30);
	}

	/*
	 * Gestión del interfaz de salida de datos: Apertura de uno de los dos interfaces
	 */
	if( oflag == 1 )
	{
		u_int16_t udp_port;
		char *ptr;

		if( debug == 1 )
			printf("\n	@ commsnom  = %s\n", commsnom);

		ptr = strchr( commsnom, ':' );

		if( ptr == NULL )
			udp_port = UDP_PORT;
		else
		{
			*ptr = '\0';

			ptr++;

			udp_port = (u_int16_t)strtol( ptr, &endptr, 10 ); /* Base 10 */

                        /* Validación de posibles errores */

                        if( (errno == ERANGE || (errno != 0 && mpromedio == 0)) )
                        {
                               perror("strtol");
			       udp_port=UDP_PORT;
                               fprintf(stderr, "Usando puerto por defecto: %d\n", udp_port);
                        }
			else if( endptr == ptr )
                        {
                               fprintf(stderr, "No se encontró valor el puerto UDP: %d\n", udp_port);
			       udp_port=UDP_PORT;
                               fprintf(stderr, "Usando puerto por defecto: %d\n", udp_port);
                        }
		}

		if( debug == 1 )
		{
			printf("\n	@ Hostname  = %s\n", commsnom);
			printf("\n	@ UDP port  = %d\n", udp_port);
		}

		/*
		 * Generamos el fichero de configuración para Telemetry View
		 */
		{
			FILE * tf;
			FILE * fp;
			int i,j;
			int ndatos = 0;

			sprintf( fnom, "telemetry_%d.txt", getpid() );

			tf = fopen( fnom, "w" );
			if( tf == NULL )
			{
				perror("Error abriendo fichero de configuración de Telemetry viewer :" );
				exit(19);
			}

			fp = fmemopen( tv_buffer, TV_BUFFER_LEN, "w" );
			if( fp == NULL )
			{
				perror("Error abriendo buffer de configuración de Telemetry viewer :" );
				exit(18);
			}

			fprintf( tf, TV_INIT, udp_port );

			fprintf( fp, TV_LOCATION, ndatos, "#", "#", "Muestras", genera_color() );
			ndatos++;

			for( i = 0; i < salidas; i++ )
			{
				for( j = 0; j < parametros[i].nops; j++ )
				{
					if( parametros[i].obdii_flag == 1 )
					{
						char desc[40];

						sprintf( desc, "%s_0x%02X", parametros[i].pid, parametros[i].obdii_pid );
						fprintf( fp, TV_LOCATION, ndatos, desc, ( parametros[i].operaciones[j].signo == 1 ? "S" : "U" ),
								parametros[i].operaciones[j].manejador, genera_color() );
					}
					else
						fprintf( fp, TV_LOCATION, ndatos, parametros[i].pid, ( parametros[i].operaciones[j].signo == 1 ? "S" : "U" ),
								parametros[i].operaciones[j].manejador, genera_color() );
					ndatos++;
				}
			}

			fclose( fp );

			fprintf( tf, TV_LOC, ndatos );
			fprintf( tf, "%s", tv_buffer );
			fprintf( tf, TV_END );

			fclose( tf );

			printf("Fichero %s generado con éxito.\n", fnom );
		}

		/*
	 	 * Inicializamos la conexión UDP
	 	 */
		udp_init( commsnom, udp_port );
	}
	else
	{
		/*
	 	 * Apertura de los ficheros a generar
	 	 */
		for( i = 0; i < salidas; i++ )
		{
			if( parametros[i].obdii_flag == 1 )

				sprintf( fnom, "%X_%02X_%s", parametros[i].npid, parametros[i].obdii_pid, argnom );
			else
			{
				int len = strlen( argnom );
				int j = 0;

				/* eliminamos el path, si lo hubiera, convirtiendo / en _ ... */

				while ( j <= len )
				{
					if( argnom[j] == '/' )
						argnom[j] = '-';
					j++;
				}

				sprintf( fnom, "%s-%s", parametros[i].pid, argnom );
			}
	
 			parametros[i].oarchivo = fopen( fnom,"w");
 			parametros[i].nom_fichero = strdup( fnom );

 			if (parametros[i].oarchivo == NULL )
			{
				perror("Error: ");
 				exit(20+i);
			}
		}
	}

	/*
	 * Inicio del proceso
	 */
	if( iflag == 1 )
	{
		/*
		 * Proceso de entrada de datos por interfaz CAN
		 */
	        struct can_frame frm;
        	struct sockaddr_can addr;
        	int ret;
        	socklen_t len;
		int i, j, enviado;
		char udp_data[UDP_DATA] = { 0 };
		FILE  * fp;

		unsigned long int contador = 0L;

		for (;;)
		{
        		ret = recvfrom(sk, &frm, sizeof(struct can_frame), 0, (struct sockaddr *)&addr, &len);

        		if (ret < 0)
			{
                		perror("Fallo en la recepción de datos del CAN bus. recvfrom()");
                		exit(7);
        		}

			for( i = 0; i < salidas; i++ )
				if( parametros[i].npid == frm.can_id ) 	/* Comprobamos si es uno de los PIDs en los que estamos interesados */
					for( j = 0; j < 8; j++ )	/* y si es así, obtenemos el valor de cada uno de los bytes de la trama */
						parametros[i].byte[j] = frm.data[j];

			if( parametros[0].npid == frm.can_id )
			{
				fp = fmemopen( udp_data, UDP_DATA, "w" );
	
				assert( fp );

				fprintf( fp, "%ld,", contador );

				contador++;

				for( i = 0; i < salidas; i++ )
				{
					if( parametros[i].obdii_flag == 1)
					{
						if( parametros[i].byte[1] != ( 0x40 | oservice ) )
							break;

						if( parametros[i].byte[2] != parametros[i].obdii_pid )
							continue;
					}

					for( j = 0; j < parametros[i].nops; j++ )
 						fprintf( fp, "%.3f,", parametros[i].operador[j]( &parametros[i].operaciones[j],  parametros[i].byte ) );
				}

 				fprintf( fp, "\n" );
	
				fclose( fp );

				len = strlen( udp_data );

		        	enviado = sendto(sockfd, udp_data, len, MSG_CONFIRM, (const struct sockaddr *) &serv,  sizeof(serv));

				if( debug == 1 )
					printf("%s\n", udp_data );

        			if (enviado < 0 )
		                	perror("Fallo enviando datos por UDP: sendto()");
			}
		} /* for (;;) */
	}
	else
	{
		/*
		 * Proceso de entrada por fichero de texto
		 */
 		while (feof(iarchivo) == 0)
 		{
			char hex[5];
			char *ptr;
			long val;
			long tiempo;
			int escribe;
	
 			fgets(caracteres,100,iarchivo);
			caracteres[11]='\0';
			caracteres[18]='\0';
			caracteres[28]='\0';

			tiempo = strtol( &caracteres[1], NULL, 10 );

			if( base == 0L )
				base = tiempo;
	
			tiempo = tiempo - base;
	
			if( tiempo >= tiempo_i - VC_PROMEDIO_INI && tiempo < tiempo_f )
			{
				if( tiempo >= tiempo_i )
					escribe = 1;
				else
					escribe = 0;

				for( i = 0; i < salidas; i++ )
				{
					int j;
	
					if( 0 == strcmp( parametros[i].pid, &caracteres[25] ) ) /* Comprobamos si es uno de los PIDs en los que estamos interesados */
					{
						for( j = 0; j < 8; j++ ) /* obtenemos el valor de cada uno de los bytes de la trama */
						{
							hex[0]=caracteres[29+j*2];
							hex[1]=caracteres[29+j*2+1];
							hex[2]='\0';
	
 							val = strtol( hex, &ptr, 16 );
							parametros[i].byte[j] = (u_int8_t) val;
						}
	
						if( parametros[i].obdii_flag == 1)
						{
							if( parametros[i].byte[1] != ( 0x40 | oservice ) )
								break;

							if( parametros[i].byte[2] != parametros[i].obdii_pid )
								continue;
						}

						if( escribe == 1 )
 							fprintf(parametros[i].oarchivo, "%ld.%s", tiempo, &caracteres[12] );
	
						for( j = 0; j < parametros[i].nops; j++ )
						{
							float fv =  parametros[i].operador[j]( &parametros[i].operaciones[j],  parametros[i].byte );

							if( escribe == 1 )
 								fprintf(parametros[i].oarchivo, " %f", fv );
						}
	
						if( escribe == 1 )
 							fprintf(parametros[i].oarchivo, "\n" );
					}
				} /* for */
			} /* if */
		} /* while */
	
        	fclose(iarchivo);
	}

	/*
	 * Cerramos los ficheros de salida de datos
	 */
	for( i = 0; i < salidas; i++ )
        	fclose(parametros[i].oarchivo);

	/*
	 * Elaboración del shell script que invoca a GNUPlot
	 */

	sprintf( fnom, "%d.sh", getpid() );
	sprintf( caracteres, "./%s", fnom );

 	sarchivo= fopen( fnom,"w");
 	if ( sarchivo == NULL )
	{
		perror("Error: abriendo fichero de shell");
 		exit(60);
	}

	fprintf( sarchivo, "#!/bin/bash\n" );
	fprintf( sarchivo, "#\n" );
	fprintf( sarchivo, "\n" );

	for( i = 0; i < salidas; i++ )
	{
		int j;

		for( j = 0; j < parametros[i].nops; j++ )
		{
			fprintf( sarchivo, "gnuplot -p << EOF\n" );
			fprintf( sarchivo, "set grid\n" );
			fprintf( sarchivo, "set y2tics\n" );
			fprintf( sarchivo, "set ytics nomirror\n" );
			fprintf( sarchivo, " set label '%s' at screen 0.01, 0.98 font 'Arial,8'\n", argnom );

			if( parametros[i].obdii_flag == 1 )
				fprintf( sarchivo, "set title 'CAN ID 0x%s - OBDII 0x%02X'\n", parametros[i].pid, parametros[i].obdii_pid );
			else
				fprintf( sarchivo, "set title 'CAN ID 0x%s'\n", parametros[i].pid );

			if( flag2 == 1)
			{
				if( j < parametros[i].nops - 1 )
				{
					fprintf( sarchivo, "plot '%s' using 1:%d with %s axes x1y1 title '%s %s',", parametros[i].nom_fichero, j+2, trazo, 
										(parametros[i].operaciones[j].signo == 1 ? "S" : "U"),
										parametros[i].operaciones[j].manejador );
					j++;
					fprintf( sarchivo, "'%s' using 1:%d with %s  axes x1y2 title '%s %s'\n", parametros[i].nom_fichero, j+2, trazo, 
										(parametros[i].operaciones[j].signo == 1 ? "S" : "U"),
										parametros[i].operaciones[j].manejador );
				}
				else
					fprintf( sarchivo, "plot '%s' using 1:%d with %s title '%s %s'\n", parametros[i].nom_fichero, j+2, trazo, 
										(parametros[i].operaciones[j].signo == 1 ? "S" : "U"),
										parametros[i].operaciones[j].manejador );
			}
			else
				fprintf( sarchivo, "plot '%s' using 1:%d with %s title '%s %s'\n", parametros[i].nom_fichero, j+2, trazo,
										(parametros[i].operaciones[j].signo == 1 ? "S" : "U"),
										parametros[i].operaciones[j].manejador );

			fprintf( sarchivo, "EOF\n\n" );
		}
	}

	fclose( sarchivo );

	if( chmod( fnom, S_IXUSR | S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH ) ) /* Damos permisos de ejecucióni al propietario: 0x744 */
	{
		printf("Error: No se pueden dar permisos de ejecución a %s", fnom );
	}
	else
	{
		/* Ejecutamos el script */
		system( caracteres );
	}

	/*
	 * Eliminamos los ficheros si se ha invocado con -r
	 */
	if( rflag )
	{
		remove( caracteres );

		for( i = 0; i < salidas; i ++ )
			remove( parametros[i].nom_fichero );
	}

	return 0;
}

